-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-11-2020 a las 16:40:06
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `si_votingo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidacies`
--

CREATE TABLE `candidacies` (
  `id` int(11) NOT NULL,
  `photo` blob DEFAULT NULL,
  `candidatetype_idfk` int(11) NOT NULL,
  `studentlist_idfk` int(11) NOT NULL,
  `id_statusfk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `candidacies`
--

INSERT INTO `candidacies` (`id`, `photo`, `candidatetype_idfk`, `studentlist_idfk`, `id_statusfk`) VALUES
(1, NULL, 1, 15, 1),
(2, NULL, 1, 8, 1),
(3, NULL, 1, 22, 1),
(4, NULL, 2, 12, 1),
(5, NULL, 2, 21, 1),
(6, NULL, 2, 10, 1),
(7, NULL, 3, 7, 1),
(8, NULL, 3, 18, 1),
(9, NULL, 3, 23, 1),
(10, NULL, 2, 1, 1),
(11, NULL, 1, 4, 1),
(12, NULL, 3, 2, 1),
(13, NULL, 1, 13, 1),
(14, NULL, 2, 19, 1),
(15, NULL, 1, 9, 1),
(16, NULL, 4, 24, 8),
(17, NULL, 4, 25, 1),
(18, NULL, 4, 26, 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `candidate_list`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `candidate_list` (
`id` int(11)
,`Candidate` varchar(61)
,`candidatetype_name` varchar(20)
,`grade_name` varchar(20)
,`code` int(4)
,`photo` blob
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidate_types`
--

CREATE TABLE `candidate_types` (
  `id` int(11) NOT NULL,
  `candidatetype_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `candidate_types`
--

INSERT INTO `candidate_types` (`id`, `candidatetype_name`) VALUES
(1, 'Personero'),
(2, 'Contralor'),
(3, 'Cabildante'),
(4, 'Voto en blanco');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `candidatos_propuestas`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `candidatos_propuestas` (
`id` int(11)
,`photo` blob
,`candidatetype_idfk` int(11)
,`studentlist_idfk` int(11)
,`id_statusfk` int(11)
,`proposal_tittle` varchar(30)
,`proposal_description` varchar(200)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `code` int(4) NOT NULL,
  `grade_idfk` int(11) NOT NULL,
  `user_idfk` int(11) NOT NULL,
  `wday_idfk` int(11) NOT NULL,
  `eprocess_idfk` int(11) DEFAULT NULL,
  `status_idfk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `courses`
--

INSERT INTO `courses` (`id`, `code`, `grade_idfk`, `user_idfk`, `wday_idfk`, `eprocess_idfk`, `status_idfk`) VALUES
(1, 1, 2, 1, 1, 1, 8),
(2, 101, 2, 4, 1, 1, 8),
(3, 301, 4, 5, 1, 1, 8),
(4, 401, 5, 6, 1, 1, 8),
(5, 501, 6, 7, 1, 1, 8),
(6, 601, 7, 8, 1, 1, 8),
(7, 701, 8, 9, 1, 1, 8),
(8, 801, 9, 10, 1, 1, 8),
(9, 901, 10, 11, 1, 1, 8),
(10, 1001, 11, 12, 1, 1, 8),
(11, 1101, 12, 13, 1, 1, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `electoral_processes`
--

CREATE TABLE `electoral_processes` (
  `id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status_idfk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `electoral_processes`
--

INSERT INTO `electoral_processes` (`id`, `start_date`, `end_date`, `status_idfk`) VALUES
(1, '2020-10-01', '2020-10-20', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `grade_name` varchar(20) NOT NULL,
  `level_idfk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `grades`
--

INSERT INTO `grades` (`id`, `grade_name`, `level_idfk`) VALUES
(1, 'Transicion', 1),
(2, 'Primero', 2),
(3, 'Segundo', 2),
(4, 'Tercero', 2),
(5, 'Cuarto', 2),
(6, 'Quinto', 2),
(7, 'Sexto', 3),
(8, 'Septimo', 3),
(9, 'Octavo', 3),
(10, 'Noveno', 3),
(11, 'Decimo', 3),
(12, 'Once', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL,
  `level_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `levels`
--

INSERT INTO `levels` (`id`, `level_name`) VALUES
(1, 'Preescolar'),
(2, 'Primaria'),
(3, 'Secundaria');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `list_candidate`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `list_candidate` (
`id` int(11)
,`candidate` varchar(61)
,`photo` blob
,`candidatetype_name` varchar(20)
,`grade_name` varchar(20)
,`code` int(4)
,`wday_name` varchar(20)
,`number_votes` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `numberstudent`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `numberstudent` (
`id` int(11)
,`code` int(4)
,`grade_idfk` int(11)
,`user_idfk` int(11)
,`wday_idfk` int(11)
,`eprocess_idfk` int(11)
,`status_idfk` int(11)
,`idpro` int(11)
,`wday_name` varchar(20)
,`grade_name` varchar(20)
,`status_name` varchar(25)
,`numberStudents` bigint(21)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proposals`
--

CREATE TABLE `proposals` (
  `id` int(11) NOT NULL,
  `proposal_tittle` varchar(30) NOT NULL,
  `proposal_description` varchar(200) NOT NULL,
  `candidacy_idfk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proposals`
--

INSERT INTO `proposals` (`id`, `proposal_tittle`, `proposal_description`, `candidacy_idfk`) VALUES
(21, 'Refrigerios', 'refrigerios todo el día', 1),
(22, 'mas descansos', 'Muchos descansos\r\n', 1),
(23, 'mas torneos', 'muchos torneos en el año', 1),
(24, 'Mas danielas', 'Muchas danielas\r\n', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `role_name`) VALUES
(1, 'Administrador'),
(2, 'Docente'),
(3, 'Estudiante'),
(4, 'Candidatura');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `scrutinies`
--

CREATE TABLE `scrutinies` (
  `id` int(11) NOT NULL,
  `candidacy_idfk` int(11) NOT NULL,
  `number_votes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `scrutinies`
--

INSERT INTO `scrutinies` (`id`, `candidacy_idfk`, `number_votes`) VALUES
(1, 1, 7),
(2, 2, 4),
(3, 3, 2),
(4, 4, 3),
(5, 5, 2),
(6, 6, 4),
(7, 7, 2),
(8, 8, 1),
(9, 9, 5),
(10, 10, 0),
(11, 11, 0),
(12, 12, 0),
(13, 13, 0),
(14, 14, 0),
(15, 15, 0),
(16, 16, 0),
(17, 17, 0),
(18, 18, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `statuses`
--

CREATE TABLE `statuses` (
  `id` int(11) NOT NULL,
  `status_name` varchar(25) NOT NULL,
  `typestatus_idfk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `statuses`
--

INSERT INTO `statuses` (`id`, `status_name`, `typestatus_idfk`) VALUES
(1, 'Habilitado', 1),
(2, 'Inhabilitado', 1),
(3, 'Iniciado', 2),
(4, 'Finalizado', 2),
(5, 'Voto', 3),
(6, 'No Voto', 3),
(7, 'Inasistencia', 3),
(8, 'Activo', 4),
(9, 'Inactivo', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `student_listing`
--

CREATE TABLE `student_listing` (
  `id` int(11) NOT NULL,
  `user_idfk` int(11) NOT NULL,
  `course_idfk` int(11) NOT NULL,
  `status_idFK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `student_listing`
--

INSERT INTO `student_listing` (`id`, `user_idfk`, `course_idfk`, `status_idFK`) VALUES
(1, 14, 7, 6),
(2, 15, 10, 6),
(3, 16, 2, 6),
(4, 17, 2, 6),
(6, 18, 2, 6),
(7, 19, 2, 6),
(8, 20, 3, 6),
(9, 21, 3, 6),
(10, 22, 4, 6),
(12, 23, 5, 6),
(13, 24, 6, 6),
(14, 25, 7, 6),
(15, 26, 7, 6),
(16, 27, 7, 6),
(17, 28, 8, 6),
(18, 29, 8, 6),
(19, 30, 9, 6),
(20, 31, 10, 6),
(21, 32, 11, 6),
(22, 33, 11, 6),
(23, 53, 2, 6),
(24, 55, 11, 8),
(25, 58, 11, 6),
(26, 59, 11, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_statuses`
--

CREATE TABLE `type_statuses` (
  `id` int(11) NOT NULL,
  `typestatus_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `type_statuses`
--

INSERT INTO `type_statuses` (`id`, `typestatus_name`) VALUES
(1, 'Usuario'),
(2, 'Proceso Electoral'),
(3, 'Listado Estudiantil'),
(4, 'Curso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `document` bigint(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  `cellphone` bigint(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status_idfk` int(11) NOT NULL,
  `voto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `document`, `password`, `cellphone`, `email`, `status_idfk`, `voto`) VALUES
(1, 'Jesus David', 'Rodriguez Chamorro', 1000350348, 'test123', 3229840101, 'JdRodriguez@gmail.com', 1, 0),
(2, 'Sergio David', 'Jacome Rodriguez', 1000234229, 'test123', 3119456412, 'SdJacome2003@gmail.com', 1, 0),
(3, 'Brayan Alexander', 'Suarez Ropero', 1000349500, 'test123', 3228993343, 'BaSuarez@gmail.com', 1, 0),
(4, 'Gabriel Stiven', 'Velandia Ochoa', 1000345325, 'test123', 3112323421, 'GsVelandia@gmail.com', 1, 0),
(5, 'Aiden Davi', 'Cortez Gomez', 1000999888, 'test123', 3224564341, 'AdavidCortez@gmail.com', 1, 0),
(6, 'Yeison Alexander', 'Farfan Peralta', 1000456454, 'test123', 3224563212, 'YalexanderFarfan@gmail.com', 1, 0),
(7, 'Zoila Sara', 'Vaca Diaz', 1000234323, 'test123', 3223453212, 'ZsVaca@gmail.com', 1, 0),
(8, 'Maria Paula', 'Soto Oliveros', 1000345328, 'test123', 3229453212, 'MpSoto@gmail.com', 1, 0),
(9, 'Camilo Esteban', 'Zuluaga Viloria', 1000345345, 'test123', 3221232212, 'Gsvelandia899@misena.edu.co', 1, 0),
(10, 'Alejadro David', 'Munevar Cortez', 1000234221, 'test123', 3229821323, 'AdMunevar@gmail.com', 1, 0),
(11, 'Victor Hugo', 'Dominguez Pelaez', 10002342211, 'test123', 3114321234, 'VhDominguez@gmail.com', 1, 0),
(12, 'Pablo Jose', 'Narciso Mina', 1000345327, 'test123', 3224532311, 'PjNarciso@gmail.com', 1, 0),
(13, 'David Guillermo', 'Rodriguez Aguirre', 10002342342, 'test123', 3110345634, 'gjacomenso2530@gmail.com', 1, 0),
(14, 'El pepe', 'Paez Rocha', 1000234210, 'test123', 3224563453, 'eiden2434@gmail.com', 1, 0),
(15, 'Jorge Fernando', 'Galindo Villanueva', 1000344333, 'test123', 3229234312, 'rodriguezaiden362221@gmail.com', 1, 0),
(16, 'Octavio Andres', 'Bautista Garzon', 1000444333, 'test123', 3229234312, 'JGrarzon3@gmail.com', 1, 0),
(17, 'Jhon Esteban', 'Suarez Villa', 10003468908, 'test123', 3224563212, 'kalent5456@gmail.com', 1, 0),
(18, 'Walter Mauricio', 'Zanabria Paez', 1000345322, 'test123', 3224443343, 'WmZanabria@gmail.com', 1, 0),
(19, 'Camilo Samuel ', 'Waltero Fez', 1000345321, 'test123', 3221234312, 'CsWaltero@gmail.com', 1, 0),
(20, 'Ivan Dario', 'Cabrera Ropero', 1024487239, 'test123', 3208006769, 'dario-cabrera@hotmail.com', 1, 0),
(21, 'Luis Alfredo', 'Suarez Murillo', 80385224, 'test123', 310385224, 'katesanchez299@gmail.com', 1, 0),
(22, 'Betty Yaneth', 'Ropero', 70254319, 'test123', 315274894, 'beropero44@gmail.com', 1, 0),
(23, 'Andre Nicolai', 'Londo?o Benitez', 100245796, 'test123', 321574326, 'andrelondo?o2001@gmail.com', 1, 0),
(24, 'Jose Luis', 'Londo?o Benitez', 100785423, 'test123', 3102005487, 'rodriguezaiden361@gmail.com', 1, 1),
(25, 'Sergio Paulo', 'Cortes Lopez', 100056987, 'test123', 3156009485, 'sergiopaulo07@gmail.com', 1, 0),
(26, 'Jhon Anderson', 'Rodriguez Sosa', 100254898, 'test123', 320584712, 'sosarodriguez@gmail.com', 1, 0),
(27, 'Marisol', 'Villa Ramirez', 10054896, 'test123', 310203645, 'mary2003@gmail.com', 1, 0),
(28, 'Stefany Sofia', 'Amado Lopez', 1000785241, 'test123', 321500698, 'enana2007@gmail.com', 1, 0),
(29, 'Diego Santiago', 'Ropero Lopez', 1000333111, 'test123', 321500698, 'Dsantiago2007@gmail.com', 1, 0),
(30, 'laura Sara', 'Ropero Lopez', 1000444111, 'test123', 321500699, 'nanis-0505@hotmail.com', 1, 0),
(31, 'Sara Sofia', 'Trilleras Duarte', 1000665111, 'test123', 3229432100, 'Sarita@gmail.com', 1, 0),
(32, 'Andrea Sofia', 'Terres Torres', 1000665555, 'test123', 3222223343, 'AndreaSofria@gmail.com', 1, 0),
(33, 'Alexander luis', 'Diaz Torres', 1000555555, 'test123', 3112223321, 'AlexDiaz@gmail.com', 1, 0),
(34, 'Camilo Jose', 'Jimenez Suazo', 1000334334, 'test123', 323111080, 'CJimenez@gmail.com', 1, 0),
(35, 'jose Jose', 'Jimenez Suazo', 100003444, 'test123', 323111050, 'JJimenez@gmail.com', 1, 0),
(36, 'Jhon Esteban', 'Rico Ruiz', 1000888765, 'test123', 323111040, 'JRico@gmail.com', 1, 0),
(37, 'Daniel Stiven', 'Tique Rocha', 1000123098, 'test123', 323111020, 'DTique@gmail.com', 1, 0),
(38, 'Pepe David', 'Reina Brocha', 1000998778, 'test123', 323111120, 'PReina@gmail.com', 1, 0),
(39, 'David Eduardo', 'De gea Suazo', 10007777777, 'test123', 323111900, 'DDgea@gmail.com', 1, 0),
(40, 'Cristian Geovanny', 'Ochoa Suarez', 1000100022, 'test123', 323111285, 'COchoa@gmail.com', 1, 0),
(41, 'Cristian Stiven', 'Ochoa Suarez', 1000454545, 'test123', 3234654912, 'COchoa1@gmail.com', 1, 0),
(42, 'Gohan San', 'Oliveira Paz', 1000222222, 'test123', 323000009, 'GSan@gmail.com', 1, 0),
(43, 'Ali Jamet', 'Paz Olivos', 1000000005, 'test123', 3229342910, 'APaz@gmail.com', 1, 0),
(44, 'Ramiro Jose', 'inirida Perez', 10123232123, 'test123', 323111282, 'RInirida@gmail.com', 1, 0),
(45, 'Xiomara karina', 'zamora sosa', 794544342, 'test123', 3232323232, 'XZamora@gmail.com', 1, 0),
(46, 'Jhon Chistensen', 'Wick Suarez', 23232323, 'test123', 3200000002, 'JWick@gmail.com', 1, 0),
(47, 'Carlos Carleto', 'Anchelotti Dominguez', 100976766, 'test123', 323232323, 'CAnchelotti@gmail.com', 1, 0),
(48, 'Santiango andres', 'Duarte mateus', 1000232232, '', 34343434, 'SDuarte23@gmail.com', 1, 0),
(49, 'Diego Jose', 'Prdoñez Fandiño', 1000221923, '', 3229421234, 'DjoseOrdonz@gmail.com', 1, 0),
(50, 'aaa', 'a', 1000444488, '', 3229423234, 'mamionga@gmail.com', 1, 0),
(51, 's', 's', 22, '', 22, 'WWW@www.com', 1, 0),
(52, 'Walter Gabriel', 'Velandia Bautista', 79911029, '', 3143718606, 'Gabos78@gmail.com', 1, 0),
(53, 'Federico jose', 'Cortez Cortez', 100345348, '', 3229423212, 'GVochoa@gmail.com', 1, 0),
(54, 'Gabriel Stiven', 'Velandia Ochoa', 11235566, 'test1234', 66655445, 'Gsvelandia8@misena.edu.co', 1, 0),
(55, 'Voto En Blanco P', '', 0, '0', 0, 'N/A', 1, 0),
(58, 'Voto en blanco C ', '', 1, '0', 0, 'N/AA', 1, 0),
(59, 'Voto en blanco Cb', '', 2, '02', 2, 'N/AAA', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usurol`
--

CREATE TABLE `usurol` (
  `user_idfk` int(11) NOT NULL,
  `role_idfk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usurol`
--

INSERT INTO `usurol` (`user_idfk`, `role_idfk`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 3),
(26, 3),
(27, 3),
(28, 3),
(29, 3),
(30, 3),
(31, 3),
(32, 3),
(33, 3),
(34, 3),
(35, 3),
(36, 3),
(37, 3),
(38, 3),
(39, 3),
(40, 3),
(41, 3),
(42, 3),
(43, 3),
(44, 3),
(45, 3),
(46, 3),
(47, 3),
(4, 1),
(11, 1),
(5, 1),
(10, 1),
(3, 1),
(52, 2),
(53, 3),
(26, 4),
(20, 4),
(33, 4),
(23, 4),
(32, 4),
(22, 4),
(19, 4),
(29, 4),
(53, 4),
(9, 1),
(14, 4),
(54, 2),
(54, 1),
(13, 1),
(17, 4),
(15, 4),
(24, 4),
(30, 4),
(21, 4),
(55, 4),
(58, 4),
(59, 4);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_candidate_other`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vw_candidate_other` (
`id` int(11)
,`proposal_tittle` varchar(30)
,`proposal_description` varchar(200)
,`id_fk` int(11)
,`Candidato` varchar(61)
,`tipo_candidato` varchar(20)
,`grade_name` varchar(20)
,`code` int(4)
,`photo` blob
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `working_day`
--

CREATE TABLE `working_day` (
  `id` int(11) NOT NULL,
  `wday_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `working_day`
--

INSERT INTO `working_day` (`id`, `wday_name`) VALUES
(1, 'Mañana'),
(2, 'Tarde');

-- --------------------------------------------------------

--
-- Estructura para la vista `candidate_list`
--
DROP TABLE IF EXISTS `candidate_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `candidate_list`  AS  select `c`.`id` AS `id`,concat(`u`.`name`,' ',`u`.`last_name`) AS `Candidate`,`ct`.`candidatetype_name` AS `candidatetype_name`,`g`.`grade_name` AS `grade_name`,`cr`.`code` AS `code`,`cd`.`photo` AS `photo` from ((((((`users` `u` join `student_listing` `sl` on(`sl`.`user_idfk` = `u`.`id`)) join `candidacies` `cd` on(`cd`.`id` = `sl`.`user_idfk`)) join `candidacies` `c` on(`c`.`studentlist_idfk` = `sl`.`id`)) join `candidate_types` `ct` on(`ct`.`id` = `c`.`candidatetype_idfk`)) join `courses` `cr` on(`cr`.`id` = `sl`.`course_idfk`)) join `grades` `g` on(`g`.`id` = `cr`.`grade_idfk`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `candidatos_propuestas`
--
DROP TABLE IF EXISTS `candidatos_propuestas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `candidatos_propuestas`  AS  select `c`.`id` AS `id`,`c`.`photo` AS `photo`,`c`.`candidatetype_idfk` AS `candidatetype_idfk`,`c`.`studentlist_idfk` AS `studentlist_idfk`,`c`.`id_statusfk` AS `id_statusfk`,`p`.`proposal_tittle` AS `proposal_tittle`,`p`.`proposal_description` AS `proposal_description` from (`candidacies` `c` join `proposals` `p` on(`p`.`candidacy_idfk` = `c`.`id`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `list_candidate`
--
DROP TABLE IF EXISTS `list_candidate`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `list_candidate`  AS  select `c`.`id` AS `id`,concat(`u`.`name`,' ',`u`.`last_name`) AS `candidate`,`c`.`photo` AS `photo`,`ct`.`candidatetype_name` AS `candidatetype_name`,`g`.`grade_name` AS `grade_name`,`co`.`code` AS `code`,`w`.`wday_name` AS `wday_name`,`sc`.`number_votes` AS `number_votes` from (((((((`users` `u` join `student_listing` `sl` on(`sl`.`user_idfk` = `u`.`id`)) join `candidacies` `c` on(`c`.`studentlist_idfk` = `sl`.`id`)) join `candidate_types` `ct` on(`ct`.`id` = `c`.`candidatetype_idfk`)) join `courses` `co` on(`co`.`id` = `sl`.`course_idfk`)) join `grades` `g` on(`g`.`id` = `co`.`grade_idfk`)) join `working_day` `w` on(`w`.`id` = `co`.`wday_idfk`)) join `scrutinies` `sc` on(`sc`.`id` = `c`.`id`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `numberstudent`
--
DROP TABLE IF EXISTS `numberstudent`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `numberstudent`  AS  select `c`.`id` AS `id`,`c`.`code` AS `code`,`c`.`grade_idfk` AS `grade_idfk`,`c`.`user_idfk` AS `user_idfk`,`c`.`wday_idfk` AS `wday_idfk`,`c`.`eprocess_idfk` AS `eprocess_idfk`,`c`.`status_idfk` AS `status_idfk`,`ep`.`id` AS `idpro`,`w`.`wday_name` AS `wday_name`,`g`.`grade_name` AS `grade_name`,`s`.`status_name` AS `status_name`,count(`sl`.`user_idfk`) AS `numberStudents` from (((((`student_listing` `sl` join `courses` `c` on(`c`.`id` = `sl`.`course_idfk`)) join `electoral_processes` `ep` on(`ep`.`id` = `c`.`eprocess_idfk`)) join `working_day` `w` on(`w`.`id` = `c`.`wday_idfk`)) join `grades` `g` on(`g`.`id` = `c`.`grade_idfk`)) join `statuses` `s` on(`s`.`id` = `c`.`status_idfk`)) group by `c`.`code` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_candidate_other`
--
DROP TABLE IF EXISTS `vw_candidate_other`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_candidate_other`  AS  select `p`.`id` AS `id`,`p`.`proposal_tittle` AS `proposal_tittle`,`p`.`proposal_description` AS `proposal_description`,`p`.`candidacy_idfk` AS `id_fk`,concat(`u`.`name`,' ',`u`.`last_name`) AS `Candidato`,`ct`.`candidatetype_name` AS `tipo_candidato`,`g`.`grade_name` AS `grade_name`,`cr`.`code` AS `code`,`cd`.`photo` AS `photo` from (((((((`users` `u` join `student_listing` `sl` on(`sl`.`user_idfk` = `u`.`id`)) join `candidacies` `cd` on(`cd`.`id` = `sl`.`user_idfk`)) join `candidacies` `c` on(`c`.`studentlist_idfk` = `sl`.`id`)) join `candidate_types` `ct` on(`ct`.`id` = `c`.`candidatetype_idfk`)) join `courses` `cr` on(`cr`.`id` = `sl`.`course_idfk`)) join `grades` `g` on(`g`.`id` = `cr`.`grade_idfk`)) join `proposals` `p` on(`p`.`candidacy_idfk` = `c`.`id`)) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `candidacies`
--
ALTER TABLE `candidacies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidatetype_idfk` (`candidatetype_idfk`),
  ADD KEY `studentlist_idfk` (`studentlist_idfk`),
  ADD KEY `id_statusfk` (`id_statusfk`);

--
-- Indices de la tabla `candidate_types`
--
ALTER TABLE `candidate_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grade_idfk` (`grade_idfk`),
  ADD KEY `wday_idfk` (`wday_idfk`),
  ADD KEY `eprocess_idfk` (`eprocess_idfk`),
  ADD KEY `status_idfk` (`status_idfk`),
  ADD KEY `userCourseFK` (`user_idfk`);

--
-- Indices de la tabla `electoral_processes`
--
ALTER TABLE `electoral_processes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_idfk` (`status_idfk`);

--
-- Indices de la tabla `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `level_idfk` (`level_idfk`);

--
-- Indices de la tabla `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proposals`
--
ALTER TABLE `proposals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidacy_idfk` (`candidacy_idfk`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `scrutinies`
--
ALTER TABLE `scrutinies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidacy_idfk` (`candidacy_idfk`);

--
-- Indices de la tabla `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `typestatus_idfk` (`typestatus_idfk`);

--
-- Indices de la tabla `student_listing`
--
ALTER TABLE `student_listing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_idfk` (`user_idfk`),
  ADD KEY `course_idfk` (`course_idfk`),
  ADD KEY `status_idFK` (`status_idFK`);

--
-- Indices de la tabla `type_statuses`
--
ALTER TABLE `type_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `document` (`document`),
  ADD UNIQUE KEY `document_2` (`document`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `status_idfk` (`status_idfk`);

--
-- Indices de la tabla `usurol`
--
ALTER TABLE `usurol`
  ADD KEY `user_idfk` (`user_idfk`),
  ADD KEY `role_idfk` (`role_idfk`);

--
-- Indices de la tabla `working_day`
--
ALTER TABLE `working_day`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `candidacies`
--
ALTER TABLE `candidacies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `candidate_types`
--
ALTER TABLE `candidate_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `electoral_processes`
--
ALTER TABLE `electoral_processes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `proposals`
--
ALTER TABLE `proposals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `scrutinies`
--
ALTER TABLE `scrutinies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `student_listing`
--
ALTER TABLE `student_listing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `type_statuses`
--
ALTER TABLE `type_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT de la tabla `working_day`
--
ALTER TABLE `working_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `candidacies`
--
ALTER TABLE `candidacies`
  ADD CONSTRAINT `candidacies_ibfk_1` FOREIGN KEY (`candidatetype_idfk`) REFERENCES `candidate_types` (`id`),
  ADD CONSTRAINT `candidacies_ibfk_2` FOREIGN KEY (`studentlist_idfk`) REFERENCES `student_listing` (`id`),
  ADD CONSTRAINT `candidacies_ibfk_3` FOREIGN KEY (`id_statusfk`) REFERENCES `statuses` (`id`);

--
-- Filtros para la tabla `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`grade_idfk`) REFERENCES `grades` (`id`),
  ADD CONSTRAINT `courses_ibfk_2` FOREIGN KEY (`wday_idfk`) REFERENCES `working_day` (`id`),
  ADD CONSTRAINT `courses_ibfk_3` FOREIGN KEY (`eprocess_idfk`) REFERENCES `electoral_processes` (`id`),
  ADD CONSTRAINT `courses_ibfk_4` FOREIGN KEY (`status_idfk`) REFERENCES `statuses` (`id`),
  ADD CONSTRAINT `userCourseFK` FOREIGN KEY (`user_idfk`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `electoral_processes`
--
ALTER TABLE `electoral_processes`
  ADD CONSTRAINT `electoral_processes_ibfk_1` FOREIGN KEY (`status_idfk`) REFERENCES `statuses` (`id`);

--
-- Filtros para la tabla `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `grades_ibfk_1` FOREIGN KEY (`level_idfk`) REFERENCES `levels` (`id`);

--
-- Filtros para la tabla `proposals`
--
ALTER TABLE `proposals`
  ADD CONSTRAINT `proposals_ibfk_1` FOREIGN KEY (`candidacy_idfk`) REFERENCES `candidacies` (`id`);

--
-- Filtros para la tabla `scrutinies`
--
ALTER TABLE `scrutinies`
  ADD CONSTRAINT `scrutinies_ibfk_1` FOREIGN KEY (`candidacy_idfk`) REFERENCES `candidacies` (`id`);

--
-- Filtros para la tabla `statuses`
--
ALTER TABLE `statuses`
  ADD CONSTRAINT `statuses_ibfk_1` FOREIGN KEY (`typestatus_idfk`) REFERENCES `type_statuses` (`id`);

--
-- Filtros para la tabla `student_listing`
--
ALTER TABLE `student_listing`
  ADD CONSTRAINT `student_listing_ibfk_1` FOREIGN KEY (`user_idfk`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `student_listing_ibfk_2` FOREIGN KEY (`course_idfk`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `student_listing_ibfk_3` FOREIGN KEY (`status_idFK`) REFERENCES `statuses` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`status_idfk`) REFERENCES `statuses` (`id`);

--
-- Filtros para la tabla `usurol`
--
ALTER TABLE `usurol`
  ADD CONSTRAINT `usurol_ibfk_1` FOREIGN KEY (`user_idfk`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `usurol_ibfk_2` FOREIGN KEY (`role_idfk`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
